#!/bin/sh -e

eval "$( while read -r l
do
    echo "export $l"
done \
    < opbot.env )"

./opbot.py
