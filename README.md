# OpBot

A simple Matrix bot that informs moderators and admins whenever someone mentions the bot's name.

Essentially a fork of [Tiny Matrix Bot](https://github.com/4nd3r/tiny-matrix-bot) but with some code removed and some added. 