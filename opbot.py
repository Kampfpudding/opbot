#!/usr/bin/env python3
# pylint: disable=missing-docstring,no-self-use,invalid-name,broad-except,too-many-instance-attributes

import asyncio
import logging
import os
import re
import sys
import time
import traceback

from nio import AsyncClient, SyncError, SyncResponse, InviteMemberEvent, RoomMessageText

logger = logging.getLogger("OpBot")


class OpBot:
    async def on_error(self, response):
        logger.error(response)
        if self._client:
            await self._client.close()
        sys.exit(1)

    _initial_sync_done = False

    async def on_sync(self, _response):
        if not self._initial_sync_done:
            self._initial_sync_done = True
            for room in self._client.rooms:
                logger.info("room %s", room)
            logger.info("initial sync done, ready for work")

    accept_invites = None

    async def on_invite(self, room, event):
        if not self.accept_invites:
            user_domain = re.escape(self.user_id.split(":")[1])
            self.accept_invites = f":{user_domain}$"
        if not re.search(self.accept_invites, event.sender, re.IGNORECASE):
            logger.info("invite %s %s ignored", event.sender, room.room_id)
            return
        logger.info("invite %s %s", event.sender, room.room_id)
        await self._client.join(room.room_id)

    _last_event_timestamp = time.time() * 1000

    async def on_message(self, room, event):
        # TODO Timeout
        await self._client.update_receipt_marker(room.room_id, event.event_id)
        mention_template = f'<a href="https://matrix.to/#/{self.user_id}">\\w+</a>.*'
        logger.info("Got message")
        logger.info(event.formatted_body)

        if event.sender == self._client.user_id:
            return
        if event.server_timestamp <= self._last_event_timestamp:
            return
        self._last_event_timestamp = event.server_timestamp

        # No mention if it hasn't a formatted body.
        if not event.formatted_body:
            return

        if re.match(mention_template, event.formatted_body):
            # await self._client.room_typing(room.room_id, True)
            body = "I'm getting an adult. Now be patient :)"
            logger.debug("send %s %s", room.room_id, body)
            await self._client.room_send(
                room_id=room.room_id,
                message_type="m.room.message",
                content={"msgtype": "m.text", "body": body},
            )

    homeserver = None
    access_token = None
    user_id = None
    _client = None

    async def run(self):
        logger.info("connect %s", self.homeserver)
        self._client = AsyncClient(self.homeserver)
        self._client.access_token = self.access_token
        self._client.user_id = self.user_id
        self._client.device_id = "OpBot"
        self._client.add_response_callback(self.on_error, SyncError)
        self._client.add_response_callback(self.on_sync, SyncResponse)
        self._client.add_event_callback(self.on_invite, InviteMemberEvent)
        self._client.add_event_callback(self.on_message, RoomMessageText)
        await self._client.sync_forever(30000)
        await self._client.close()


if __name__ == "__main__":
    if "OB_DEBUG" in os.environ:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    try:
        OB = OpBot()
        OB.homeserver = os.environ["OB_HOMESERVER"]
        OB.access_token = os.environ["OB_ACCESS_TOKEN"]
        OB.user_id = os.environ["OB_USER_ID"]
        if "OB_ACCEPT_INVITES" in os.environ:
            OB.accept_invites = os.environ["OB_ACCEPT_INVITES"]
        asyncio.run(OB.run())
    except Exception:
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)
    except KeyboardInterrupt:
        sys.exit(0)
